## Command line output
Navigate to this project folder in terminal and run one of the following commands:
```
php console render:tree
```
```
php console render:tree S
```
```
php console render:tree M
```
```
php console render:tree L
```
```
php console render:star
```
```
php console render:star S
```
```
php console render:star M
```
```
php console render:star L
```

## Browser output
Navigate to **./public/star.php** or **./public/tree.php** in your browser.

You can change a value of **$sizeName** variable in these files to change shape's size. It's set to "M" size by default.

# Notes
For some reasons the design for L size star shape breaks the pattern established by two other sizes. The code could be slightly cleaner if that particular design would follow the established pattern.