<?php

namespace App\Exceptions;

use Exception;

/**
 * Class InvalidShapeSizeException
 * @package App\Exceptions
 */
class InvalidShapeSizeException extends Exception
{
}