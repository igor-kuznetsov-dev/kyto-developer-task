<?php

namespace App\Shapes;

/**
 * Class AbstractShapeFactory
 * @package App\Shapes
 */
abstract class AbstractShapeFactory
{
    /**
     * @param null|string $sizeName
     * @return ShapeInterface
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    abstract public function createShape(?string $sizeName): ShapeInterface;
}