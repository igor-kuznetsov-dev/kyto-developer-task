<?php

namespace App\Shapes;

/**
 * Class StarFactory
 * @package App\Shapes
 */
class StarFactory extends AbstractShapeFactory
{
    /**
     * @param null|string $sizeName
     * @return ShapeInterface
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    public function createShape(?string $sizeName): ShapeInterface
    {
        return new Star($sizeName);
    }
}