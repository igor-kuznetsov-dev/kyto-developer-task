<?php

namespace App\Shapes;

use App\Exceptions\InvalidShapeSizeException;

/**
 * Class AbstractShape
 * @package App
 */
abstract class AbstractShape implements ShapeInterface
{
    const CHAR_BORDER = '+';
    const CHAR_MAIN = 'x';
    const CHAR_SPACE = ' ';

    const SIZES = [
        'S' => 5,
        'M' => 7,
        'L' => 11
    ];

    /**
     * @var string
     */
    protected $sizeName;

    /**
     * @var int
     */
    protected $sizeValue;

    /**
     * AbstractShape constructor.
     * @param null|string $sizeName
     * @throws InvalidShapeSizeException
     */
    public function __construct(?string $sizeName)
    {
        $this->setSizeValue($sizeName);
    }

    /**
     * @param null|string $sizeName
     * @throws InvalidShapeSizeException
     */
    public function setSizeValue(?string $sizeName)
    {
        $this->sizeName = $sizeName;

        if (empty($this->sizeName)) {
            $this->sizeName = $this->getRandomSizeName();
        }

        if ($this->isValidSize()) {
            $this->sizeValue = self::SIZES[$this->sizeName];
        } else {
            throw new InvalidShapeSizeException("Invalid shape size! Allowed sizes: S, M, L.");
        }
    }

    /**
     * @return bool
     */
    private function isValidSize(): bool
    {
        return array_key_exists($this->sizeName, self::SIZES);
    }

    /**
     * @return string
     */
    private function getRandomSizeName(): string
    {
        return array_rand(self::SIZES);
    }
}