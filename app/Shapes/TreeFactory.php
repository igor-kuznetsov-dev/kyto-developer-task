<?php

namespace App\Shapes;

/**
 * Class TreeFactory
 * @package App\Shapes
 */
class TreeFactory extends AbstractShapeFactory
{
    /**
     * @param null|string $sizeName
     * @return ShapeInterface
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    public function createShape(?string $sizeName): ShapeInterface
    {
        return new Tree($sizeName);
    }
}