<?php

namespace App\Shapes;

/**
 * Interface ShapeInterface
 * @package App\Shapes
 */
interface ShapeInterface
{
    /**
     * @return string
     */
    public function render(): string;
}