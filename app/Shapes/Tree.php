<?php

namespace App\Shapes;

/**
 * Class Tree
 * @package App
 */
class Tree extends AbstractShape
{
    /**
     * @return string
     */
    public function render(): string
    {
        $rows = [];

        for ($i = 0; $i < $this->sizeValue; $i++) {
            $rows[] = $this->renderRow($i, $this->sizeValue);
        }

        return implode(PHP_EOL, $rows);
    }

    /**
     * @param int $rowNumber
     * @return string
     */
    private function renderRow(int $rowNumber): string
    {
        $rowsCount = $this->sizeValue;

        if ($this->isFirstRow($rowNumber)) {
            $spacesCount = $rowsCount - 2;
            $charsCount = 1;
            $char = self::CHAR_BORDER;
        } else {
            $spacesCount = $rowsCount - $rowNumber - 1;
            $charsCount = 2 * $rowNumber - 1;
            $char = self::CHAR_MAIN;
        }

        return str_repeat(self::CHAR_SPACE, $spacesCount) . str_repeat($char, $charsCount);
    }

    /**
     * @param int $rowNumber
     * @return bool
     */
    private function isFirstRow(int $rowNumber): bool
    {
        return $rowNumber == 0;
    }
}