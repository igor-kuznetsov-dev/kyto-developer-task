<?php

namespace App\Shapes;

/**
 * Class Star
 * @package App
 */
class Star extends AbstractShape
{
    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $maxCharsCount;

    /**
     * @return string
     */
    function render(): string
    {
        $rows = [];

        $this->limit = floor($this->sizeValue / 2);

        if ($this->sizeName == 'L') {
            $this->maxCharsCount = ($this->sizeValue - 2) + 2 * (floor(($this->sizeValue - 2) / 2) - 1);
        } else {
            $this->maxCharsCount = ($this->sizeValue - 2) + 2 * floor(($this->sizeValue - 2) / 2);
        }

        for ($i = $this->limit; $i >= -$this->limit; $i--) {
            $rows[] = $this->renderRow($i);
        }

        return implode(PHP_EOL, $rows);
    }

    /**
     * @param int $rowNumber
     * @return string
     */
    private function renderRow(int $rowNumber): string
    {
        if ($this->isFirstOrLastRow($rowNumber)) {
            $char = self::CHAR_BORDER;
            $charsCount = 1;
        } else {
            $char = self::CHAR_MAIN;
            $charsCount = abs($this->maxCharsCount - abs(4 * $rowNumber));
        }

        $spacesCount = floor(($this->maxCharsCount - $charsCount) / 2) + 1;

        $chars = str_repeat($char, $charsCount);
        $spaces = str_repeat(self::CHAR_SPACE, $spacesCount);

        if ($this->isMiddleRow($rowNumber)) {
            $output = self::CHAR_BORDER . $chars . self::CHAR_BORDER;
        } else {
            $output = $spaces . $chars;
        }

        return $output;
    }

    /**
     * @param int $rowNumber
     * @return bool
     */
    private function isFirstOrLastRow(int $rowNumber): bool
    {
        return abs($rowNumber) == $this->limit;
    }

    /**
     * @param int $rowNumber
     * @return bool
     */
    private function isMiddleRow(int $rowNumber): bool
    {
        return $rowNumber == 0;
    }
}