<?php

namespace App\Commands;

use App\Exceptions\InvalidShapeSizeException;
use App\Shapes\AbstractShapeFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RenderShapeCommand
 * @package App\Commands
 */
class RenderShapeCommand extends Command
{
    /**
     * @var AbstractShapeFactory
     */
    private $factory;

    /**
     * RenderShapeCommand constructor.
     * @param null|string $name
     * @param AbstractShapeFactory $factory
     */
    public function __construct(?string $name = null, AbstractShapeFactory $factory)
    {
        parent::__construct($name);

        $this->factory = $factory;
    }

    protected function configure()
    {
        $this->addArgument('size', InputArgument::OPTIONAL, 'Shape Size');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $size = $input->getArgument('size');

        try {
            $shape = $this->factory->createShape($size);
            $output->writeln($shape->render());
        } catch (InvalidShapeSizeException $exception) {
            $this->getApplication()->renderException($exception, $output);
        }
    }
}