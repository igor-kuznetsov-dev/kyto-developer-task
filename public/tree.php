<?php

require __DIR__.'/../vendor/autoload.php';

use App\Shapes\TreeFactory;
use App\Exceptions\InvalidShapeSizeException;

$sizeName = 'M';

try {
    $factory = new TreeFactory();
    $shape = $factory->createShape($sizeName);
} catch (InvalidShapeSizeException $exception) {
    die($exception->getMessage());
}

echo '<pre>';
echo $shape->render();
echo '</pre>';