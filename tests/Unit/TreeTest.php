<?php

namespace Tests\Unit;

use App\Shapes\TreeFactory;

/**
 * Class TreeTest
 * @package Tests
 */
class TreeTest extends TestCase
{
    /**
     * @var TreeFactory
     */
    private $factory;

    protected function setUp()
    {
        parent::setUp();

        $this->factory = new TreeFactory();
    }

    protected function tearDown()
    {
        parent::tearDown();

        unset($this->factory);
    }

    /**
     * @dataProvider shapeSizeProvider
     * @param $shapeSize
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    public function testRenderNotEmptyString($shapeSize)
    {
        $shape = $this->factory->createShape($shapeSize);

        $output = $shape->render();

        $this->assertNotEmpty($output);
        $this->assertTrue(is_string($output));
    }
}