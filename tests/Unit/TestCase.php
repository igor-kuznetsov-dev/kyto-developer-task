<?php

namespace Tests\Unit;

use App\Shapes\StarFactory;
use App\Shapes\TreeFactory;
use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package Tests\Unit
 */
class TestCase extends BaseTestCase
{
    /**
     * @return array
     */
    public function shapeSizeProvider(): array
    {
        return [['S'], ['M'], ['L'], [null]];
    }

    /**
     * @return array
     */
    public function invalidShapeSizeProvider(): array
    {
        return [['A'], [5]];
    }

    /**
     * @return array
     */
    public function renderShapeCommandProvider(): array
    {
        return [
            ['render:tree', new TreeFactory, 'S'],
            ['render:tree', new TreeFactory, 'M'],
            ['render:tree', new TreeFactory, 'L'],
            ['render:tree', new TreeFactory, null],
            ['render:star', new StarFactory, 'S'],
            ['render:star', new StarFactory, 'M'],
            ['render:star', new StarFactory, 'L'],
            ['render:star', new StarFactory, null],
        ];
    }
}