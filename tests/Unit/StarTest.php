<?php

namespace Tests\Unit;

use App\Shapes\StarFactory;

/**
 * Class StarTest
 * @package Tests\Unit
 */
class StarTest extends TestCase
{
    /**
     * @var StarFactory
     */
    private $factory;

    protected function setUp()
    {
        parent::setUp();

        $this->factory = new StarFactory();
    }

    protected function tearDown()
    {
        parent::tearDown();

        unset($this->factory);
    }

    /**
     * @dataProvider shapeSizeProvider
     * @param $shapeSize
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    public function testRenderNotEmptyString($shapeSize)
    {
        $shape = $this->factory->createShape($shapeSize);

        $output = $shape->render();

        $this->assertNotEmpty($output);
        $this->assertTrue(is_string($output));
    }
}