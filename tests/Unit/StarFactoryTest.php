<?php

namespace Tests\Unit;

use App\Shapes\ShapeInterface;
use App\Shapes\StarFactory;

/**
 * Class StarFactoryTest
 * @package Tests\Unit
 */
class StarFactoryTest extends TestCase
{
    /**
     * @var StarFactory
     */
    private $factory;

    protected function setUp()
    {
        parent::setUp();

        $this->factory = new StarFactory();
    }

    protected function tearDown()
    {
        parent::tearDown();

        unset($this->factory);
    }

    /**
     * @dataProvider shapeSizeProvider
     * @param $shapeSize
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    public function testCreateShape($shapeSize)
    {
        $shape = $this->factory->createShape($shapeSize);

        $this->assertInstanceOf(ShapeInterface::class, $shape);
    }

    /**
     * @dataProvider invalidShapeSizeProvider
     * @param $shapeSize
     * @expectedException \App\Exceptions\InvalidShapeSizeException
     */
    public function testInvalidShapeSizeException($shapeSize)
    {
        $this->factory->createShape($shapeSize);
    }
}