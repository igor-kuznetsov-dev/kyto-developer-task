<?php

namespace Tests\Unit;

use App\Shapes\ShapeInterface;
use App\Shapes\TreeFactory;

/**
 * Class TreeFactoryTest
 * @package Tests
 */
class TreeFactoryTest extends TestCase
{
    /**
     * @var TreeFactory
     */
    private $factory;

    protected function setUp()
    {
        parent::setUp();

        $this->factory = new TreeFactory();
    }

    protected function tearDown()
    {
        parent::tearDown();

        unset($this->factory);
    }

    /**
     * @dataProvider shapeSizeProvider
     * @param $shapeSize
     * @throws \App\Exceptions\InvalidShapeSizeException
     */
    public function testCreateShape($shapeSize)
    {
        $shape = $this->factory->createShape($shapeSize);

        $this->assertInstanceOf(ShapeInterface::class, $shape);
    }

    /**
     * @dataProvider invalidShapeSizeProvider
     * @param $shapeSize
     * @expectedException \App\Exceptions\InvalidShapeSizeException
     */
    public function testInvalidShapeSizeException($shapeSize)
    {
        $this->factory->createShape($shapeSize);
    }
}