<?php

namespace Tests\Unit;

use App\Commands\RenderShapeCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;


/**
 * Class RenderShapeCommandTest
 * @package Tests\Unit
 */
class RenderShapeCommandTest extends TestCase
{
    /**
     * @dataProvider renderShapeCommandProvider
     * @param $commandName
     * @param $factory
     * @param $shapeSize
     */
    public function testRenderNotEmptyString($commandName, $factory, $shapeSize)
    {
        $application = new Application();
        $application->add(new RenderShapeCommand($commandName, $factory));

        $command = $application->find($commandName);
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'size' => $shapeSize
        ]);

        $output = $commandTester->getDisplay();

        $this->assertNotEmpty($output);
        $this->assertTrue(is_string($output));
    }
}